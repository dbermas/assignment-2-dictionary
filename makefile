ASM=nasm
ASMFLAGS=-f elf64
LINKER=ld
PYTHON=python3


%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<


init: main.o lib.o dict.o
	$(LINKER) -o $@ $^

.PHONY: clean test
clean:
	rm -rf *.o init

test:
	
	$(PYTHON) test.py
