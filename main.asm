%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUFFER_SIZE 255

section .rodata
	NOT_FOUND_ERROR: db 'Error: Value not found', 0
	BUFFER_OVERFLOW_ERROR: db 'Error: Max buffer length 255', 0

section .bss
	buffer: resb 256

section .text
	global _start

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word;stdin

	test rax, rax
	je .overflow
	
	mov rdi, rax
	mov rsi, first_word
	call find_word;from dict.inc
	test rax, rax
	jz .not_found

	mov rdi, rax
	
	push rdi
	call string_length; from lib.inc
	pop rdi
	
	add rdi, rax
	inc rdi; null-terminate
	
	call print_string;stdout
	jmp .exit

.not_found:
	mov rdi, NOT_FOUND_ERROR
    	jmp .error

.overflow:
	mov rdi, BUFFER_OVERFLOW_ERROR
	jmp .error
	
.error:
	call print_error;stderr

.exit:
	call exit


	
