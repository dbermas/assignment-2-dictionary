%include "lib.inc"

section .text
global find_word

find_word:
	test rsi,rsi
	je .exit
	
	mov rcx, rsi
.loop:
	
	lea rsi, [rcx+8] 
	call string_equals
	test rax, rax
	jnz .found

	mov rcx, [rcx]
	test rcx, rcx
	jnz .loop
	
	xor rax, rax
	jmp .exit

.found:
	mov rax, rsi

.exit:
	ret
