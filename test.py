import subprocess 
import unittest

class TestDictionary(unittest.TestCase): 
    def run_prog(self, stdin):
        program_path = "./init"  
        process = subprocess.Popen(
            [program_path],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            shell=True  
        )
        stdout, stderr = process.communicate(input=stdin)
        return stdout.strip(), stderr.strip()  
        
    def cheking(self, words):
        for stdin, promice_stdout, promice_stderr in words:
            with self.subTest(stdin=stdin):
                result_stdout, result_stderr = self.run_prog(stdin)
                self.assertEqual(result_stdout, promice_stdout)
                self.assertEqual(result_stderr, promice_stderr)
        
    def test_valid_words(self):
        words = [
            ("rose", "first", ""), 
            ("iris", "second", ""),
            ("tulip","third","")
        ]
            
        self.cheking(words)
        
    def test_word_not_found(self):
        words = [
            ("tree", "", "Error: Value not found")
        ]
        self.cheking(words)

    def test_buffer_overflow(self):
        words = [
            ("ITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMOITMO", "", "Error: Max buffer length 255")
        ]
        self.cheking(words)

if __name__ == "__main__":
    unittest.main()
