section .text




%define TEXT_MINUS     45

%define SYS_EXIT 60


%define SYS_READ        0
%define SYS_WRITE       1


%define FD_STDIN        0
%define FD_STDOUT       1
%define FD_STDERR       2

%define ASCII_SPACE     0x20
%define ASCII_TAB       0x9
%define ASCII_NEW_LINE  0xA

global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy




 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi + rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	xor rax, rax
	push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret
	
	
print_error:
    push    rdi
    call    string_length
    mov     rdx, rax
    mov     rax, SYS_WRITE
    pop     rsi
    mov     rdi, FD_STDERR
    syscall
    ret
    
; Принимает код символа и выводит его в stdout
print_char:
	xor rax, rax
	push rdi
	mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ASCII_NEW_LINE
    jmp print_char
    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    
	
	mov rax, rdi	
	sub rsp, 32	 
	mov byte [rsp + 31], 0
	lea rsi, [rsp + 31]
	mov rcx, 10
	
	
    .loop:
    	xor rdx, rdx
	div rcx
	add dl, '0'
	dec rsi        
        mov byte [rsi], dl 
	test rax, rax
	jne .loop 	
		
	mov rdi, rsi     
	call print_string   
	add rsp, 32
	ret
; Выводит знаковое 8-байтовое число в десятичном формате 

 print_int:
    test rdi,rdi
	jge .print
	push rdi
	mov rdi, TEXT_MINUS
	call print_char
	pop rdi
	neg rdi
	.print:
		call print_uint
	ret
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
     .loop:
		mov al, byte[rsi]
		cmp byte[rdi], al
		jne .not_equals
		cmp byte[rdi], 0
		je .equals
		inc rsi
		inc rdi
		jmp .loop
	.not_equals:
		xor rax, rax
		ret
	.equals:
		mov rax, 1
		ret


; читает один символ из stdin
; возвращает: rax -- код символа (0 если достигнут конец потока)
read_char:	

	xor rax, rax
	mov rdi, 0
	lea rsi, [rsp-1]
	mov rdx, 1
	syscall
	
	cmp rax, 1          
	jne .eof
    	movzx rax, byte [rsp-1]
    	
    	ret
    .eof:
	xor rax, rax    		
    	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word: ;
    mov rax, 0        
    mov rcx, 0          
    .loop:
        cmp rcx, rsi    
        jge .overflow  

        push rcx
        push rdi      
        push rsi 
        call read_char  
        pop rsi         
        pop rdi 
        pop rcx

        cmp rax, ASCII_SPACE   
        je .space       
        cmp rax, ASCII_TAB 
        je .space
        cmp rax, ASCII_NEW_LINE
        je .space 

        mov [rdi+rcx], rax 
        
        test rax, rax      
        je .end         
        inc rcx         
        jmp .loop      
    .space:
        test rcx, rcx     
        jne .end       
        jmp .loop       
    
    .end:               
        mov rdx, rcx
        mov rax, rdi
        ret

    .overflow:         
        xor rax, rax; return 0
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
	mov rcx, 0 
    mov rsi, 0 
    mov r10, 10
	
    .loop:
    	movzx r9, byte[rdi+rcx]     
        sub r9, '0'                  
        cmp r9, 0  ; more  or equal 0             
        jl .end 
        cmp r9, 9; less or equal 9
        jg .end 
        mul r10                     
        add rax, r9                 
        inc rcx                    
        jmp .loop 
		
    .end:
		mov rdx, rcx
    	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
 
    cmp byte [rdi], TEXT_MINUS
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	.loop:
		cmp rax, rdx
		je .overflow
		mov cl, byte[rdi + rax]
		mov byte[rsi + rax], cl
		
		test cl, cl
		je .copied
		inc rax
	jmp .loop
	.overflow:
		xor rax, rax
		ret
	.copied:
		ret
